var version = "Beta v.0.2";

//disable "Upgrade Premium" for Moderators
$('ul#userbaritems li a[href$="http://www.elitepvpers.com/theblackmarket/premium/"]').parent().hide();

//Debug
$('div.smallfont span.time:first').after('<br>EES ' + version +' loaded');

//http://stackoverflow.com/a/18729931
function toUTF8Array(str) {
    var utf8 = [];
    for (var i=0; i < str.length; i++) {
        var charcode = str.charCodeAt(i);
        if (charcode < 0x80) utf8.push(charcode);
        else if (charcode < 0x800) {
            utf8.push(0xc0 | (charcode >> 6),
                      0x80 | (charcode & 0x3f));
        }
        else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(0xe0 | (charcode >> 12),
                      0x80 | ((charcode>>6) & 0x3f),
                      0x80 | (charcode & 0x3f));
        }
        else {
            i++;
            charcode = 0x10000 + (((charcode & 0x3ff)<<10)
                      | (str.charCodeAt(i) & 0x3ff));
            utf8.push(0xf0 | (charcode >>18),
                      0x80 | ((charcode>>12) & 0x3f),
                      0x80 | ((charcode>>6) & 0x3f),
                      0x80 | (charcode & 0x3f));
        }
    }
    return utf8;
}

var userid = $('#userbaritems a[href^="members/"]').attr('href').match(/members\/(\d+)-\S+\.html/);
userid = userid[1];
var usernick = $('#userbaritems a[href^="members/"]').html();

var pageDesc = 'none';
var url = window.location.href;

var urlsplit = url.split('/');
/*
if ($('strong:contains("Announcement")').length){
    pageDesc = 'forumsection';
}
if (pageDesc == 'forumsection') && urlsplit[urlsplit.length - 1].indexOf(".html") > -1){
    pageDesc = 'forumsection2';
}
*/

chrome.runtime.sendMessage({"title": usernick, "msg": userid});

console.log('Url: ' + url);
console.log('Page: ' + pageDesc);

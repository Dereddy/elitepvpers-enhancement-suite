# Elitepvpers Enhancement Suite
Cool browser extension to enhance your elitepvpers experience


Install
-------------
Drag the EES.xpi into your Firefox browser (Version 42 with Developer Tools).


Building
-------------
Run 'build.bat' under windows. Unix support comming soon.


ToDo
-------------
- [x] Removal of "Upgrade to Premium" for moderators
- [ ] Never Ending Epvp
- [ ] User Tags
- [ ] Full Ignore
- [ ] Full Chrome Browser Support
- [ ] Build Support for Unix


License
-------------
![CC by-nc](https://i.creativecommons.org/l/by-nc/4.0/88x31.png) 

    This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International License. 
    To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/ 
    or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

####In short

You are free to:

 - Share
 - Adapt

Under the following terms:

 - Attribution
 - NonCommercial

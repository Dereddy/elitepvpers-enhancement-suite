if (pageDesc == 'forumsection'){
    var page = 2;
} else {
    var page = url.slice(-5, -4);
}


if (pageDesc == 'forumsection' || pageDesc == 'forumsection2'){
    $('table #threadslist').after('<table class="tborder" id="threadslist" align="center" border="0" cellpadding="6" cellspacing="0" width="100%"><tr><td class="thead" colspan="7">Never Ending Elitepvpers Module</td></tr>' +
    '<tr><td style="text-align:center; font-size:1.5em;" id="neverEndingLoading"><a href="">&gt; Click to load the next page &lt;</a><br>' + url + 'index' + page + '.html' + '</td></tr>' +
    '</table>');

    function neverEnding() {
        $('#neverEndingLoading').html('<img src="http://www.elitepvpers.com/forum/images/misc/progress.gif">  Loading ...');
    }

    $('#neverEndingLoading').click(function(e) {
        e.defaultPrevented();
        neverEnding();
    });

    var alreadyloading = false;

    $(window).scroll(function() {
        if ($('body').height() - 600 <= ($(window).height() + $(window).scrollTop())) {
            if (alreadyloading == false) {
                var nextUrl = url + 'index' + page + '.html';
                alreadyloading = true;
                $('#threadslist').after('<table id="threadslist' + page + '"></table>');
                $('#threadslist' + page).load( 'ajax/' + nextUrl + ' #threadslist' );
                alreadyloading = false;
                page++;
            }
        }
    });
}
